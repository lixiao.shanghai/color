import React, {Suspense, useEffect} from "react";
import classes from './App.css';
import Canvas from "./components/Canvas/Canvas";
import {useIntl} from 'react-intl';
import appMessages from './app.locale.json'
import sharedMessages from './json/shared.locale.json'
import { Route, Switch, Redirect } from "react-router-dom";

const CanvasReplica = React.lazy(() => import('./components/CanvasReplica/CanvasReplica'))


const App = () => {
    const intl = useIntl();
    useEffect(() => {
        document.title = intl.formatMessage(sharedMessages.pageTitle)
    })

    let routes = (
        <Suspense fallback={<div>Loading</div>}>
            <Switch>
                <Route exact path="/">
                    <Canvas/>
                </Route>
                <Route path="/replica">
                    <CanvasReplica/>
                </Route>
                <Redirect to="/"/>
            </Switch>
        </Suspense>

    )

    return (
        // IntlProvider doesn't actually care about locale
        // whatever translation document is provided to message will show
        <div className={classes.App}>
            <h1>
                {
                    intl.formatMessage(appMessages.colourApp)
                }
            </h1>
            {routes}
        </div>
    )
}

export default App
