import React from 'react';
import ReactDOM from 'react-dom';
import AppParent from "./AppParent";
import TranslateProvider from "./context/TranslateContext";
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <BrowserRouter>
        <TranslateProvider>
            <AppParent/>
        </TranslateProvider>
    </BrowserRouter>
    ,
    document.getElementById('root')
);
