const translateReducer = (state, action) =>{
    console.log('reducer received: ', action)
    switch (action.type){
        case 'UPDATE_LOCALE':
            return {
                ...state,
                locale: action.locale,
                messages: action.messages
            }
        default:
            return state
    }
}

export default translateReducer
