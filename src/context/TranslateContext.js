/**
 * Wrap App from index.js with <TranslateContext> so that react-intl renders formatText in the selected language-locale
 * across the app.
 * */
import React, {createContext, useReducer} from "react";
import translateReducer from "./translateReducer";

const DEFAULT_LOCALE = 'en-AU';

const initialState = {
    locale: DEFAULT_LOCALE,
    messages: false
}

const TranslateProvider = ({children}) => {
    const [state, dispatch] = useReducer(translateReducer, initialState)
    return (
        <TranslateContext.Provider
            value={[state, dispatch]}>
            {children}
        </TranslateContext.Provider>
    )
}

export const TranslateContext = createContext(initialState)
export default TranslateProvider


// const TranslateProvider = ({children}) => {
//     const [state, dispatch] = useReducer(translateReducer, initialState)
//     return (
//         <TranslateContext.Provider
//             value={[state, dispatch]}>
//             {children}
//         </TranslateContext.Provider>
//     )
// }
//
// export const TranslateContext = createContext(initialState)
// export default TranslateProvider

// export const TranslateContext = createContext({
//     locale: DEFAULT_LOCALE,
//     messages: false,
//     updateLocale: () => {},
//     updateMessages: () => {},
// })
// const TranslateProvider = ({children}) => {
//     const [locale, setLocale] = useState(DEFAULT_LOCALE)
//     const [messages, setMessages] = useState(false)
//     const updateLocale = (locale) => {
//         setLocale(locale)
//     }
//     const updateMessages = (messages) => {
//         setMessages(messages)
//     }
//
//     return (
//         <TranslateContext.Provider
//             value={{
//                 locale,
//                 messages,
//                 updateLocale,
//                 updateMessages,
//             }}
//         >
//             {children}
//         </TranslateContext.Provider>
//     )
// }

// export default TranslateProvider;
