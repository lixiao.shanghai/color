import React, {useContext, useRef, useEffect} from "react";
import App from './App';
import {IntlProvider} from "react-intl";
import {TranslateContext} from "./context/TranslateContext";


const AppParent = () => {
    const [state] = useContext(TranslateContext)

    return (
        <IntlProvider messages={state.messages} locale={state.locale} defaultLocale='en-AU'>
            {console.log('AppParent Rendered')}
            <App/>
        </IntlProvider>
    )
}

export default AppParent;
