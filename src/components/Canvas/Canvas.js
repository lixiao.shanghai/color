import React, {useContext, useEffect, useRef, useState} from "react";
import classes from './Canvas.css';
import {colorSteps} from '../../shared/color';
import Button from "../UI/Button/Button";
import ReactCountdownClock from 'react-countdown-clock'
import {TranslateContext} from "../../context/TranslateContext";
import {useIntl} from 'react-intl';
import canvasMessages from './canvas.locale.json';
import {NavLink} from 'react-router-dom';
import sharedMessages from "../../json/shared.locale.json";
import {loadLocaleData} from "../../context/mockApiCall"

const rowCount = 128
const columnCount = 256

// general all 15-bit colors
const steps = colorSteps()

const Canvas = () => {
    const randomIndex = () => {
        return Math.floor(Math.random() * 32768);
    }

    const [initialColorIndex, setInitialColorIndex] = useState(randomIndex())

    const handleGenerateAnother = () => {
        setInitialColorIndex(randomIndex())
    }

    const canvasRef = useRef();
    useEffect(() => {
        // get ref to canvas
        const canvasObj = canvasRef.current;
        const ctx = canvasObj.getContext('2d');

        // pick a random color for the center point
        // const initialColorIndex = randomIndex()
        const centerColor = steps[initialColorIndex]

        // calculate distance in worker, so that main ui is not blocked
        const worker = new Worker('../../canvas.worker.js', {type: 'module'});
        worker.postMessage({steps, centerColor});

        // do the rest after receiving results
        worker.onmessage = e => {
            let outputColors = e.data

            // flatten matrix into data array
            let imageData = ctx.createImageData(columnCount, rowCount)
            let k = 0;
            for (let i = 0; i < rowCount; i++) {
                for (let j = 0; j < columnCount; j++) {
                    imageData.data[k] = outputColors[i][j][0]
                    imageData.data[k + 1] = outputColors[i][j][1]
                    imageData.data[k + 2] = outputColors[i][j][2]
                    imageData.data[k + 3] = 255 // alpha
                    k += 4
                }
            }
            // draw on canvas
            ctx.putImageData(imageData, 20, 10)
        }

    }, [initialColorIndex])

    const [state, dispatch] = useContext(TranslateContext)
    const intl = useIntl()
    const onLocaleChangeHandler = (e) => {
        const locale = e.target.value;
        loadLocaleData(locale).then(
            res => {
                res && res.default ?
                    dispatch({type: 'UPDATE_LOCALE', locale, messages: res.default}) :
                    dispatch({type: 'UPDATE_LOCALE', locale, messages: false})
            }
        )

    }

    return (
        <div className={classes.Canvas}>
            <p>{intl.formatMessage(canvasMessages.today, {ts: Date.now()})}</p>
            <NavLink className={classes.Btn} to="/replica">{intl.formatMessage(sharedMessages.lazy)}</NavLink>
            <select onChange={onLocaleChangeHandler}
                    defaultValue={state.locale}
            >
                <option value="en-AU">English (Australia)</option>
                <option value="zh-CN">中文简体 - Simplified Chinese</option>
            </select>
            <canvas className={classes.Canvas} ref={canvasRef}/>
            <Button clicked={handleGenerateAnother} btnType="Success">{
                intl.formatMessage(canvasMessages.generateImage)
            }</Button>
            <p>{intl.formatMessage(canvasMessages.webWorker)}</p>
            <p>{intl.formatMessage(canvasMessages.countdown)}</p>
            <p>{intl.formatMessage(canvasMessages.source)}
                <a href="https://gitlab.com/lixiao.shanghai/color">GitLab</a></p>
            <p>{intl.formatMessage(canvasMessages.pipeline)}
                <a href="https://gitlab.com/lixiao.shanghai/color/-/blob/main/.gitlab-ci.yml">Pipeline</a></p>
            <ReactCountdownClock/>
        </div>
    )
}

export default Canvas

