/**
 * this file is written to extract internationalization messages from the staff-app and coax them into formatjs
 * compliant data structure. The output can then be fed to formatjs to compile into AST format json for localization
 * purposes.
 *
 * extracted messages are output to lang/en-AU.json; tweak by changing outputFile variable below
 * compiled messages goes into compiled-lang/en-AU.json; tweak in cli command
 *
 * extract by running the next line in command line
 * node extract.js
 * compile in command line with the next line, remember to add "compile": "formatjs compile" to your scripts in
 * package.json
 * npm run compile -- lang/en-AU.json --ast --out-file compiled-lang/en-AU.json
 * */

const fs = require('fs');
const path = require('path');

// tell me where you want to start traversing to find .json files
let projectDir = path.normalize(__dirname + '/src')

let filesToExtract = []

// explore all folders to add json to filesToExtract
const exploreDirectory = (dir) => {
    // get the list of files in the projectDir
    let files = fs.readdirSync(dir)
    files.forEach(file => {
        // get stat from each file in dir
        let stat = fs.statSync(path.join(dir, file))
        if (stat.isFile() && file.match(/(\w*)\.locale\.json$/)) {
            // a file is found and name ends in .json, add to filesToExtract
            filesToExtract.push(path.join(dir, file))
        } else if (stat.isDirectory()) {
            // a directory is found, go into directory and explore
            // Note: I assume loops due to use of symbolic links do not exist
            // if loops exist, modify exploreDirectory to keep track of visited nodes
            exploreDirectory(path.join(dir, file))
        }
    })
}

// start exploring from projectDir
exploreDirectory(projectDir)
console.log('Files to extract: ', filesToExtract)

let messages = {}

// read each file and spread it into messages
filesToExtract.forEach(file => {
    let raw = fs.readFileSync(file);
    let parsed = JSON.parse(raw);
    messages = {
        ...messages,
        ...parsed
    }
})

// format messages into format.js compliant structure for npm run compile to work properly
let extracted = {}
for (const [key, value] of Object.entries(messages)) {
    let defaultMessage = value.defaultMessage
    let id = value.id
    let description = value.description
    extracted[id] = {defaultMessage}
    description && (extracted[id].description = description)
}

// write out extracted messages
let outputFile = '/lang/en-AU.json'
fs.writeFile(path.normalize(__dirname + outputFile), JSON.stringify(extracted), function (err) {
    if (err) return console.log(err)
    console.log('extracted messages')
})
